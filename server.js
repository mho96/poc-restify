const restify = require('restify');
const contents = require('./controllers/ContentController');
const authentication = require('./middlewares/AuthenticationMiddleware');
const authorization = require('./middlewares/AuthorizationRoleBasedMiddleware');

const server = restify.createServer();

server.pre(authentication);

server.use(restify.plugins.bodyParser({
    maxBodySize: 1000,
}));

server.get('/contents', 
    authorization('admin'),
    (req, res, next) => contents.getMany(req, res, next)
);

server.post('/contents', (req, res, next) => contents.create(req, res, next));
server.get('/contents/:id', (req, res, next) => contents.getOne(req, res, next));
server.put('/contents/:id', (req, res, next) => contents.replace(req, res, next));
server.patch('/contents/:id', (req, res, next) => contents.edit(req, res, next));
server.del('/contents/:id', (req, res, next) => contents.delete(req, res, next));

server.listen(8080, () => console.log('server running at localhost:8080'));