const contentService = require('../services/ContentService');
const errs = require('restify-errors');
const uniqid = require('uniqid');

class ContentConroller {
    constructor(contentService) {
        this.contentService = contentService;
    }

    async getOne(req, res, next) {
        try {
            const result = await this.contentService.getOne(req.params.id);
            if (result === null) {
                return next(new errs.NotFoundError(`content ${req.params.id} not found`));
            }
            res.send(200, JSON.parse(result));
        } catch (err) {
            return next(new Error(err));
        }
        return next();
    }

    async getMany(req, res, next) {
        try {
            const result = await this.contentService.getMany();
            if (result === null) {
                res.send(204)
            } else {
                const list = Object.values(result).map(val => JSON.parse(val));
                res.send(200, list);
            }
        } catch (err) {
            return next(new Error(err));
        }
        return next();
    }

    async create(req, res, next) {
        try {
            const fields = {
                title: req.body.title, 
                body: req.body.body
            };
            const id = uniqid();
            const created = Math.floor(Date.now()/1000);
            const payload = { id: id , created: created, ...fields };
            await this.contentService.create(id, JSON.stringify(payload));
            res.send(201, { id });
        } catch (err) {
            return next(new Error(err));
        }
        return next();
    }

    async replace(req, res, next) {
        try {
            const fields = {
                title: req.body.title, 
                body: req.body.body
            };
            const created = Math.floor(Date.now()/1000);
            const payload = { id: req.params.id , created: created, ...fields };
            await this.contentService.create(
                req.params.id,
                JSON.stringify(payload)
            );
            res.send(200, payload);
        } catch (err) {
            return next(new Error(err));
        }
        return next();
    }

    async edit(req, res, next) {
        try {
            const existing = await this.contentService.getOne(req.params.id);
            const fields = Object.keys(req.body).filter(
                key => !['id', 'created'].includes(key)
            ).reduce((acc, curr) => {
                return { ...acc, [curr]: req.body[curr] }
            }, {});

            const merge = { ...JSON.parse(existing), ...fields };
            const created = Math.floor(Date.now()/1000);
            const payload = { id: req.params.id , ...merge, created: created };
            
            await this.contentService.create(
                req.params.id,
                JSON.stringify(payload)
            );
            res.send(200, payload);
        } catch (err) {
            return next(new Error(err));
        }
        return next();
    }

    async delete(req, res, next) {
        try {
            const result = await this.contentService.delete(req.params.id);
            const message = result 
            ? `content ${req.params.id} has been deleted`
            : `content ${req.params.id} has not been deleted`
            res.send(200, { message });
        } catch (err) {
            return next(new Error(err));
        }
        return next();
    }
}

module.exports = new ContentConroller(contentService);