const errs = require('restify-errors');

const authorization = (role) => {
    return (req, res, next) => {
        if (req.roles && req.roles.includes(role)) next();
        else next(new errs.ForbiddenError('insufficient permission'));
    }
}

module.exports = authorization;