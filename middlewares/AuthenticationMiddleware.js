const jwt = require('jsonwebtoken');
// a ne pas reproduire à la maison lol
const config = require('../config.json'); 
const errs = require('restify-errors');

const authenticationMiddleware = (req, res, next) => {
    const token = req.header('Authorization', null);
    const { secret } = config;
    if (!token) {
        next(new errs.UnauthorizedError('Authentication failed'))
    } else {
        jwt.verify(token, secret, { algorithms: ['HS256'] }, (err, payload) => {
            if (err) {
                next(new errs.UnauthorizedError('Authentication failed'));
            } else {
                const roles = [...payload.roles];
                req.roles = roles;
                next();
            }
        });
    }
}

module.exports = authenticationMiddleware;